﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HDY_20113_Jinatchapak.Startup))]
namespace HDY_20113_Jinatchapak
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
