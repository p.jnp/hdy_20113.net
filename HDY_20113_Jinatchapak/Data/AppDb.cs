namespace HDY_20113_Jinatchapak.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using HDY_20113_Jinatchapak.Models;
    using Microsoft.AspNet.Identity.EntityFramework;

    public partial class AppDb : IdentityDbContext<ApplicationUser>
    {
        public static AppDb Create() 
        {
            return new AppDb();
        }
        public AppDb(): base("name=AppDb")
        {
        }

        public DbSet<Images> Images { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Shipper> Shippers { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        //public System.Data.Entity.DbSet<HDY_20113_Jinatchapak.Models.Images> Images { get; set; }
    }
}
