﻿using System.Web;
using System.Web.Mvc;

namespace HDY_20113_Jinatchapak
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
