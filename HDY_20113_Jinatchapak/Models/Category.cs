﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HDY_20113_Jinatchapak.Models
{
    [Table("categories")]
    public class Category
    {
        [Column("category_id")]
        /* PK */
        [Key]
        /* auto increment */
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        /* show msg view*/
        [Display(Name = "ประเภทสินค้า ID")]
        public int CategoryId { get; set; }

        [Column("category_name")]
        /* Not Null*/
        [Required]
        /* show msg view*/
        [Display(Name = "ชื่อประเภทสินค้า")]
        public string CategoryName { get; set; }

        [Column("active_flag ")]
        /* show msg view*/
        [Display(Name = "คำอธิบายประเภทสินค้า")]
        public string ActiveFlag { get; set; } = "Y";

        [Column("created_by")]
        [Display(Name = "สร้างโดย")]
        [StringLength(50, ErrorMessage = "Created By")]
        public string CreatedBy { get; set; }

        /* show msg view*/
        [Column("created_date ")]
        [Display(Name = "วันที่สร้าง")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreatedDate { get; set; } = DateTime.Now;


        [Column("updated_by")]
        [Display(Name = "อัพเดตโดย")]
        [StringLength(50, ErrorMessage = "Created By")]
        public string UpdatedBy { get; set; }

        /* show msg view*/
        [Column("updated_date ")]
        [Display(Name = "วันที่อัพเดต")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime UpdatedDate { get; set; } = DateTime.Now;

    }
}