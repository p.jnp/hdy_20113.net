﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HDY_20113_Jinatchapak.Models
{
    [Table("products")]
    public class Product
    {

        [Column("product_id")]
        /* PK */
        [Key]
        /* auto increment */
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        /* show msg view*/
        [Display(Name = "สินค้า ID")]
        public int ProductId { get; set; }

        [Column("product_name")]
        /* Not Null*/
        [Required]
        /* show msg view*/
        [Display(Name = "ชื่อสินค้า")]
        public string ProductName { get; set; }

        [Column("quantity_per_unit")]
        /* show msg view*/
        [Display(Name = "ปริมาณสินค้าต่อหน่วย")]
        /* Not Null*/
        [Required]
        public string QuantityPerUnit { get; set; }

        [Column("unit_price")]
        /* show msg view*/
        [Display(Name = "ราคาต่อหน่วย")]
        /* Not Null*/
        [Required]
        /* specify type DB*/
        //[Column(TypeName = "decimal(10, 2)")]
        public decimal UnitPrice { get; set; }

        [Column("unit_in_stock")]
        /* show msg view*/
        [Display(Name = "จำนวนสินค้าในสต๊อก")]
        /* Not Null*/
        [Required]
        public int UnitInStock { get; set; }


        [Column("discontinued")]
        /* show msg view*/
        [Display(Name = "ปิดจำหน่าย")]
        public int Discontinued { get; set; }

        [Column("category_id")]
        /* Relationnal FK*/
        public int CategoryId { get; set; } //CategoryId
        public Category category { get; set; } //Category

        public virtual ICollection<Images> img { get; set; } = new HashSet<Images>();
    }
}