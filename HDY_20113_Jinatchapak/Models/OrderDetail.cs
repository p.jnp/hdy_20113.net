﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HDY_20113_Jinatchapak.Models
{
    [Table("order_details")]
    public class OrderDetail
    {

        [Key, Column("order_id", Order = 0)]
        public int OrderId { get; set; }

        [Key, Column("product_id", Order = 1)]
        public int ProductId { get; set; }

        public virtual Order Order { get; set; }

        public virtual Product Product { get; set; }

        [Column("unit_price")]
        /* show msg view*/
        [Display(Name = "ราคาต่อหน่วย")]
        public decimal UnitPrice { get; set; }

        [Column("quantity")]
        /* show msg view*/
        [Display(Name = "จำนวน")]
        public int Quantity { get; set; }


        [Column("discount")]
        [Display(Name = "ส่วนลด")]
        [Range(0, double.MaxValue, ErrorMessage = "The discount must be value greater than 0")]
        public decimal Discount { get; set; }



        public decimal Total => (decimal)Quantity * UnitPrice;

    }
}