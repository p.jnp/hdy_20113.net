﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HDY_20113_Jinatchapak.Models
{
    [Table("orders")]
    public class Order
    {

        [Column("order_id")]
        /* PK */
        [Key]
        /* auto increment */
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        /* show msg view*/
        [Display(Name = "คำสั่งซื้อ ID")]
        public int OrderId { get; set; }

        [Column("order_date")]
        /* Not Null*/
        [Required]
        /* show msg view*/
        [Display(Name = "วันสั่งซื้อ")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime OrderDate { get; set; } = DateTime.Now;


        [Column("ship_name")]
        [Required]
        /* show msg view*/
        [Display(Name = "ชื่อที่จัดส่ง")]
        public string ShipName { get; set; }

        [Column("ship_address")]
        [Required]
        /* show msg view*/
        [Display(Name = "ที่อยู่ที่จัดส่ง")]
        public string ShipAddress { get; set; }


        [Column("ship_postal_code")]
        /* Not Null*/
        [Required]
        /* show msg view*/
        [Display(Name = "รหัสไปรษณีย์")]
        public string ShipPostalCode { get; set; }

        public string ShipCountry { get; set; }

        [Column("user_id ")]
        /* Relationnal FK*/
        public int UserId { get; set; }



        [Column("shipper_id")]
        /* Relationnal FK*/
        [Display(Name = "ผู้จัดส่ง Id")]
        public int ShipperId { get; set; } //CategoryId
        public Shipper shipper { get; set; } //CategoryId = 1, CategoryName = eiei

        //public virtual ICollection<OrderDetail> OrderDetails { get; set; } = new HashSet<OrderDetail>();
        [Required]
        public virtual ICollection<OrderDetail> OrderItem { get; set; }
            = new HashSet<OrderDetail>();

        public decimal SubTotal => OrderItem.Sum(x => x.Total);
        public decimal VatAmount => Math.Round(SubTotal * 0.07m, 2, MidpointRounding.AwayFromZero);
        public decimal NetTotal => SubTotal + VatAmount;

    }
}