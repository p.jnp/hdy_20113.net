﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HDY_20113_Jinatchapak.Models
{
    public class Images
    {
        [Column("image_id")]
        /* PK */
        [Key]
        /* auto increment */
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        /* show msg view*/
        [Display(Name = "สินค้า ID")]
        public int ImageId { get; set; }

        [Column("image_path")]
        /* PK */
        /* show msg view*/
        [Display(Name = "รูปภาพ")]
        public string ImagePath { get; set; }

        [Column("product_id")]
        /* Relationnal FK*/
        public int ProductId { get; set; } //CategoryId
        public Product product { get; set; } //Category
    }
}