﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HDY_20113_Jinatchapak.Models
{
    [Table("shippers")]
    public class Shipper
    {
        [Column("shipper_id")]
        /* PK */
        [Key]
        /* auto increment */
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        /* show msg view*/
        [Display(Name = "ผู้จัดส่ง Id")]
        public int ShipperId { get; set; }

        [Column("company_name")]
        /* Not Null*/
        [Required]
        /* show msg view*/
        [Display(Name = "ชื่อบริษัท")]
        public string CompanyName { get; set; }


        [Column("phone")]
        /* Not Null*/
        [Required]
        /* show msg view*/
        [Display(Name = "เบอร์ติดต่อ")]
        public string Phone { get; set; }
    }
}