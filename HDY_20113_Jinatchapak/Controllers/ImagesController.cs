﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HDY_20113_Jinatchapak.Data;
using HDY_20113_Jinatchapak.Models;

namespace HDY_20113_Jinatchapak.Controllers
{
    public class ImagesController : Controller
    {
        private AppDb db = new AppDb();

        // GET: Images
        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("View", "Images", null);

            }
            var productId = (from img in db.Images.AsNoTracking()
                            join pro in db.Products.AsNoTracking() on img.ProductId equals pro.ProductId
                            where img.ImageId == id
                            select pro.ProductId).FirstOrDefault();
            var images = db.Images.Find(productId);
            ViewBag.ProductName = db.Products.Find(id).ProductName;
            return View(images);
        }

        // GET: Images
        public ActionResult View()
        {
            var image = db.Images.Include(p => p.product);
            return View(image.ToList());
        }

        // GET: Images/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Images images = db.Images.Find(id);
            if (images == null)
            {
                return HttpNotFound();
            }
            return View(images);
        }

        // GET: Images/Create
        [Route("{controller}/create/{ProductId}")]
        public ActionResult Create(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "ProductName");
            return View();
        }

        // POST: Images/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Route("{controller}/create/{ProductId}")]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ImageId,ImagePath,ProductId")] Images images)
        public ActionResult Create(int id, Images images)
        {
            if (ModelState.IsValid)
            {
                var p = db.Products.Find(id);
                db.Images.Add(images);
                db.SaveChanges();
                //return RedirectToAction("Index");
                return RedirectToAction(nameof(Details), new { id = images.ProductId });
            }

            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "ProductName", images.ProductId);
            return View(images);
        }

        // GET: Images/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Images images = db.Images.Find(id);
            if (images == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "ProductName", images.ProductId);
            return View(images);
        }

        // POST: Images/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ImageId,ImagePath,ProductId")] Images images)
        {
            if (ModelState.IsValid)
            {
                db.Entry(images).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "ProductName", images.ProductId);
            return View(images);
        }

        // GET: Images/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Images images = db.Images.Find(id);
            if (images == null)
            {
                return HttpNotFound();
            }
            return View(images);
        }

        // POST: Images/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Images images = db.Images.Find(id);
            db.Images.Remove(images);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
