﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HDY_20113_Jinatchapak.Data;
using HDY_20113_Jinatchapak.Models;
namespace HDY_20113_Jinatchapak.Controllers
{
    public class ProductsController : Controller
    {
        private AppDb db = new AppDb();

        // GET: Products
        public ActionResult Index()
        {
            var products = db.Products.Include(p => p.category);
            return View(products.ToList());
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }
        [Route("{controller}/DetailImg/{ProductId}")]
        public ActionResult DetailImg(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("View", "Images", null);

            }
            //var product = (from img in db.Images.AsNoTracking()
            //                 join pro in db.Products.AsNoTracking() on img.ProductId equals pro.ProductId
            //                 where img.ImageId == id
            //                 select pro).ToList();
            var product = db.Products.Include(p => p.img).Where(p => p.ProductId == id).FirstOrDefault();

            if (product == null) return HttpNotFound();

            //var images = db.Images.Find(product.ProductId);
            //ViewBag.ProductName = product.ProductName;
            // return View(images);
            return PartialView(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "CategoryName");
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductId,ProductName,QuantityPerUnit,UnitPrice,UnitInStock,Discontinued,ImgPath,CategoryId")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "CategoryName", product.CategoryId);
            return View(product);
        }

        // GET: Images/Create
        public ActionResult CreateImage(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.Product = db.Products.Include(p=>p.img).Where(p=> p.ProductId == id).ToList();
            //ViewBag.ProductId = new SelectList(db.Products, "ProductId", "ProductName");
            return View();
        }

        [HttpPost]
        [Route("{controller}/CreateImage/{ProductId}")]
        [ValidateAntiForgeryToken]
        //public ActionResult Create()
        public ActionResult CreateImage(int id, [Bind(Include = "ImageId,ImagePath,ProductId")] Images images)
        {
            var productId = db.Products.Find(id).ProductId;
            
            if (ModelState.IsValid)
            {
                var p = db.Products.Find(id);
                p.img.Add(images);
                db.SaveChanges();
                //return RedirectToAction("Index");
                return RedirectToAction(nameof(Details), new { id = images.ProductId });
            }

            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "ProductName", images.ProductId);
            
            return View(images);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "CategoryName", product.CategoryId);
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductId,ProductName,QuantityPerUnit,UnitPrice,UnitInStock,Discontinued,ImgPath,CategoryId")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "CategoryName", product.CategoryId);
            return View(product);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
