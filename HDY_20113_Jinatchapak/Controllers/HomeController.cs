﻿using HDY_20113_Jinatchapak.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HDY_20113_Jinatchapak.Controllers
{
    public class HomeController : Controller
    {
        private AppDb db = new AppDb();
        public ActionResult Index()
        {
            var products = db.Products.Include(p => p.category);
            return View(products.ToList());  
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Product()
        {
            var products = db.Products.Include(p => p.category);
            return View(products.ToList());
        }
    }
}