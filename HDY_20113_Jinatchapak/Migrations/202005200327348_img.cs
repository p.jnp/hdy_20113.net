namespace HDY_20113_Jinatchapak.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class img : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.products", "supplier_id", "dbo.suppliers");
            DropIndex("dbo.products", new[] { "supplier_id" });
            AddColumn("dbo.products", "img_path", c => c.Int(nullable: false));
            AlterColumn("dbo.categories", "category_name", c => c.String(nullable: false));
            AlterColumn("dbo.products", "product_name", c => c.String(nullable: false));
            DropColumn("dbo.products", "supplier_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.products", "supplier_id", c => c.Int(nullable: false));
            AlterColumn("dbo.products", "product_name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.categories", "category_name", c => c.String(nullable: false, maxLength: 10));
            DropColumn("dbo.products", "img_path");
            CreateIndex("dbo.products", "supplier_id");
            AddForeignKey("dbo.products", "supplier_id", "dbo.suppliers", "supplier_id", cascadeDelete: true);
        }
    }
}
