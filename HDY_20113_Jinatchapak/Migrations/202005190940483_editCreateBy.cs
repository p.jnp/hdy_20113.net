namespace HDY_20113_Jinatchapak.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editCreateBy : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.categories", "created_by", c => c.String(maxLength: 50));
            AlterColumn("dbo.categories", "updated_by", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.categories", "updated_by", c => c.String(maxLength: 30));
            AlterColumn("dbo.categories", "created_by", c => c.String(maxLength: 30));
        }
    }
}
