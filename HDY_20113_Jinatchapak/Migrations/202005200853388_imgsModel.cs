namespace HDY_20113_Jinatchapak.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class imgsModel : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.products", "img_path");
        }
        
        public override void Down()
        {
            AddColumn("dbo.products", "img_path", c => c.Int(nullable: false));
        }
    }
}
