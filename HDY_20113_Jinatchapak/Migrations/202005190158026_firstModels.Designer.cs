// <auto-generated />
namespace HDY_20113_Jinatchapak.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class firstModels : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(firstModels));
        
        string IMigrationMetadata.Id
        {
            get { return "202005190158026_firstModels"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
