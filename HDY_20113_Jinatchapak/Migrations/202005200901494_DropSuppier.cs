namespace HDY_20113_Jinatchapak.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropSuppier : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        image_id = c.Int(nullable: false, identity: true),
                        image_path = c.String(),
                        product_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.image_id)
                .ForeignKey("dbo.products", t => t.product_id, cascadeDelete: true)
                .Index(t => t.product_id);
            
            DropTable("dbo.suppliers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.suppliers",
                c => new
                    {
                        supplier_id = c.Int(nullable: false, identity: true),
                        company_name = c.String(nullable: false, maxLength: 100),
                        address = c.String(nullable: false),
                        postal_code = c.String(nullable: false),
                        phone = c.String(nullable: false),
                        fax = c.String(),
                    })
                .PrimaryKey(t => t.supplier_id);
            
            DropForeignKey("dbo.Images", "product_id", "dbo.products");
            DropIndex("dbo.Images", new[] { "product_id" });
            DropTable("dbo.Images");
        }
    }
}
