namespace HDY_20113_Jinatchapak.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class firstModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.categories",
                c => new
                    {
                        category_id = c.Int(nullable: false, identity: true),
                        category_name = c.String(nullable: false, maxLength: 10),
                        active_flag = c.String(name: "active_flag "),
                        created_by = c.String(maxLength: 30),
                        created_date = c.DateTime(name: "created_date ", nullable: false),
                        updated_by = c.String(maxLength: 30),
                        updated_date = c.DateTime(name: "updated_date ", nullable: false),
                    })
                .PrimaryKey(t => t.category_id);
            
            CreateTable(
                "dbo.order_details",
                c => new
                    {
                        order_id = c.Int(nullable: false),
                        product_id = c.Int(nullable: false),
                        unit_price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        quantity = c.Int(nullable: false),
                        discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => new { t.order_id, t.product_id })
                .ForeignKey("dbo.orders", t => t.order_id, cascadeDelete: true)
                .ForeignKey("dbo.products", t => t.product_id, cascadeDelete: true)
                .Index(t => t.order_id)
                .Index(t => t.product_id);
            
            CreateTable(
                "dbo.orders",
                c => new
                    {
                        order_id = c.Int(nullable: false, identity: true),
                        order_date = c.DateTime(nullable: false),
                        ship_name = c.String(nullable: false),
                        ship_address = c.String(nullable: false),
                        ship_postal_code = c.String(nullable: false),
                        ShipCountry = c.String(),
                        user_id = c.Int(name: "user_id ", nullable: false),
                        shipper_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.order_id)
                .ForeignKey("dbo.shippers", t => t.shipper_id, cascadeDelete: true)
                .Index(t => t.shipper_id);
            
            CreateTable(
                "dbo.shippers",
                c => new
                    {
                        shipper_id = c.Int(nullable: false, identity: true),
                        company_name = c.String(nullable: false),
                        phone = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.shipper_id);
            
            CreateTable(
                "dbo.products",
                c => new
                    {
                        product_id = c.Int(nullable: false, identity: true),
                        product_name = c.String(nullable: false, maxLength: 100),
                        quantity_per_unit = c.String(nullable: false),
                        unit_price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        unit_in_stock = c.Int(nullable: false),
                        discontinued = c.Int(nullable: false),
                        supplier_id = c.Int(nullable: false),
                        category_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.product_id)
                .ForeignKey("dbo.categories", t => t.category_id, cascadeDelete: true)
                .ForeignKey("dbo.suppliers", t => t.supplier_id, cascadeDelete: true)
                .Index(t => t.supplier_id)
                .Index(t => t.category_id);
            
            CreateTable(
                "dbo.suppliers",
                c => new
                    {
                        supplier_id = c.Int(nullable: false, identity: true),
                        company_name = c.String(nullable: false, maxLength: 100),
                        address = c.String(nullable: false),
                        postal_code = c.String(nullable: false),
                        phone = c.String(nullable: false),
                        fax = c.String(),
                    })
                .PrimaryKey(t => t.supplier_id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.order_details", "product_id", "dbo.products");
            DropForeignKey("dbo.products", "supplier_id", "dbo.suppliers");
            DropForeignKey("dbo.products", "category_id", "dbo.categories");
            DropForeignKey("dbo.orders", "shipper_id", "dbo.shippers");
            DropForeignKey("dbo.order_details", "order_id", "dbo.orders");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.products", new[] { "category_id" });
            DropIndex("dbo.products", new[] { "supplier_id" });
            DropIndex("dbo.orders", new[] { "shipper_id" });
            DropIndex("dbo.order_details", new[] { "product_id" });
            DropIndex("dbo.order_details", new[] { "order_id" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.suppliers");
            DropTable("dbo.products");
            DropTable("dbo.shippers");
            DropTable("dbo.orders");
            DropTable("dbo.order_details");
            DropTable("dbo.categories");
        }
    }
}
